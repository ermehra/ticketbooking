import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API} from '../../environments/environment';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import * as fakeDb from '@app/modules/db/fakeDbModel';
import {Movie, Slot, Theatre} from '@app/models';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private _http: HttpClient) {
  }

  getAllMovies(date) {
    return this._http.post<fakeDb.ShowTiming[]>(API.movies, date).pipe(switchMap(value => {
      console.log(value);
      const result = [];
      value.forEach(show => {

        const slot: Slot = new Slot({id: show.id, dateTime: show.dateTime});
        const movie: Movie = new Movie({id: show.movie.id, name: show.movie.name, slots: [slot]});

        const theatreIndex = result.findIndex(t => t.id === show.theatre.id);
        if (theatreIndex > -1) {

          /* Check if movie already exists in object*/
          const movieIndex = result[theatreIndex].movies.findIndex(m => m.id === show.movie.id);
          if (movieIndex > -1) {
            /* existing movie, just add slot*/
            result[theatreIndex].movies[movieIndex].slots.push(slot);
          } else {
            /* New Movie*/
            result[theatreIndex].movies.push(movie);
          }
        } else {
          /* New theatre*/
          const theatre: Theatre = new Theatre({id: show.theatre.id, name: show.theatre.name, movies: [movie]});

          result.push(theatre);
        }
      });
      console.log(result);
      return of(result);
    }));
  }
}
