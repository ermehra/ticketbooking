import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API} from '../../environments/environment';
import {map} from 'rxjs/internal/operators';
import {User} from '../modules/db/fakeDbModel/user';
import {BehaviorSubject, Observable} from 'rxjs';
import {Role} from '@app/modules/db/fakeDbModel';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loggedUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
  loggedUserObservable: Observable<User> = this.loggedUserSubject.asObservable();

  constructor(private _http: HttpClient) {
  }

  getLoggedUserValue(): User {
    if (this.isEmpty(this.loggedUserSubject.getValue())) {
      return null;
    } else {
      return new User(this.loggedUserSubject.getValue());
    }
  }

  login(credentials) {
    return this._http.post<any>(API.login, credentials)
      .pipe(map(response => {
          /*
          User successfully logged in
          Store keys localStorage for handling browser refresh
          */
          localStorage.setItem('user', JSON.stringify(response));
          this.loggedUserSubject.next(response);

          return response;
        }
      ));
  }

  isAdmin() {
    if (this.getLoggedUserValue()) {
      return this.getLoggedUserValue().type === Role.Admin;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('user');
    this.loggedUserSubject.next(null);

    location.reload(true);
  }

  isEmpty(obj) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
}
