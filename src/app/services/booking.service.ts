import {Injectable} from '@angular/core';
import {LoginService} from '@app/services';
import {API} from '../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Booking} from '@app/modules/db/fakeDbModel';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private _http: HttpClient,
              private _loginService: LoginService) {
  }

  bookTicket(slotId: number, seats: number) {
    const body = {
      slotId: slotId,
      seats: seats,
      user: this._loginService.getLoggedUserValue()
    };

    return this._http.post(API.book, body);
  }

  getBookingHistory() {
    const user = this._loginService.getLoggedUserValue();

    const httpParams = new HttpParams().set('userid', user.id + '');

    return this._http.get<Booking[]>(`${API.history}?id=${user.id}`);
  }
}
