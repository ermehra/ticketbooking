import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messageSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor() {
  }

  success(message: string, keepAfterNavigationChange = false) {
    this.messageSubject.next({type: 'success', text: message});
  }

  error(message: string, keepAfterNavigationChange = false) {
    this.messageSubject.next({type: 'error', text: message});
  }

  clear() {
    this.messageSubject.next(null);
  }

  getMessage(): Observable<any> {
    return this.messageSubject.asObservable();
  }
}
