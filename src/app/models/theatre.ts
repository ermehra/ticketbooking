import {Movie} from '@app/models';

export class Theatre {
  id: number;
  name: string;
  movies: Movie[];

  constructor(obj) {
    Object.assign(this, obj);
  }
}
