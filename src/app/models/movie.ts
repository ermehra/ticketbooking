import {Slot} from 'src/app/models';

export class Movie {
  id: number;
  name: string;
  slots: Slot[];

  constructor(obj) {
    Object.assign(this, obj);
  }
}
