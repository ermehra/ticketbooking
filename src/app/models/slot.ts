import {Moment} from 'moment';

export class Slot {

  id: number;
  dateTime: Moment;

  constructor(obj) {
    Object.assign(this, obj);
  }
}
