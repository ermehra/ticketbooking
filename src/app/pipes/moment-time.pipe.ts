import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import {Moment} from 'moment';

@Pipe({
  name: 'momentTime'
})
export class MomentTimePipe implements PipeTransform {

  transform(value: Moment, args?: any): string {
    return value.format('h:mm a');
  }

}
