import { Pipe, PipeTransform } from '@angular/core';
import {Moment} from 'moment';

@Pipe({
  name: 'momentDateTime'
})
export class MomentDateTimePipe implements PipeTransform {

  transform(value: Moment, args?: any): string {
    return value.format('MMM Do YYYY, h:mm a');
  }

}
