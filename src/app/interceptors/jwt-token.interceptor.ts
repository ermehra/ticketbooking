import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {LoginService} from '@app/services';
import {User} from '@app/modules/db/fakeDbModel/index';

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {

  constructor(private _loginService: LoginService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const loggedInUser: User = new User(this._loginService.getLoggedUserValue());
    if (loggedInUser) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer fake-jwt-token-${loggedInUser.type}`
        }
      });
    }

    return next.handle(req);
  }
}
