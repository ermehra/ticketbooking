import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {delay, dematerialize, materialize, mergeMap} from 'rxjs/internal/operators';
import {DbUserService} from '@app/modules/db/services/db-user.service';
import {DbShowTimingsService} from '@app/modules/db/services/db-show-timings.service';
import {API} from '../../environments/environment';
import {DbBookingService} from '@app/modules/db/services/db-booking.service';
import {DbMovieService} from '@app/modules/db/services/db-movie.service';
import {Role} from '@app/modules/db/fakeDbModel';
import {DbTheatreService} from '@app/modules/db/services/db-theatre.service';

@Injectable()
export class Server implements HttpInterceptor {

  constructor(
    private _dbUser: DbUserService,
    private _dbShowTimings: DbShowTimingsService,
    private _dbBooking: DbBookingService,
    private _dbMovie: DbMovieService,
    private _dbTheatre: DbTheatreService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const {url, method, headers, body} = req;

    const dBUserService = this._dbUser;
    const dbShowTimings = this._dbShowTimings;
    const dbBooking = this._dbBooking;
    const dbMovie = this._dbMovie;
    const dbTheatre = this._dbTheatre;

    // wrap in delayed observable to simulate server api call
    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());

    function handleRoute() {
      switch (true) {
        case url.endsWith(API.login) && method === 'POST':
          return login();
        case url.endsWith(API.movies) && method === 'POST':
          return getAllMovies();
        case url.endsWith(API.adminMovies) && method === 'POST':
          return getAllMoviesForAdmin();
        case url.endsWith(API.book) && method === 'POST':
          return bookTicket();
        case url.includes(API.history) && method === 'GET':
          return getHistory();
        case url.endsWith(API.listMovies) && method === 'GET':
          return getMovieList();
        case url.endsWith(API.createMovie) && method === 'POST':
          return createMovie();
        case url.endsWith(API.createTheatre) && method === 'POST':
          return createTheatre();
        case url.includes(API.shows) && method === 'GET':
          return getShows();
        case url.includes(API.shows) && method === 'POST':
          return saveShows();
        case url.includes(API.theatre) && method === 'GET':
          return getTheatres();
        /*case url.endsWith('/users') && method === 'GET':
          return getUsers();
        case url.match(/\/users\/\d+$/) && method === 'GET':
          return getUserById();
        case url.match(/\/users\/\d+$/) && method === 'DELETE':
          return deleteUser();*/
        default:
          // pass through any requests not handled above
          return next.handle(req);
      }
    }

    function login() {
      const {username, password} = body;
      const user = dBUserService.getUserByNameAndPassword(username, password);

      if (user) {
        return ok(user);
      } else {
        return unauthenticated();
      }
    }

    function getAllMovies() {
      if (!(isAuthenticated() || isAuthorized())) {
        return unauthenticated();
      }
      const filterDate = body;
      const shows = dbShowTimings.getAllShowsForDate(filterDate);
      return ok(shows);
    }

    function getAllMoviesForAdmin() {
      if (!isAuthorized()) {
        return unauthorized();
      }
      const filterDate = body;
      const shows = dbShowTimings.getAllShowsForDate(filterDate);
      return ok(shows);
    }


    function bookTicket() {
      if (!(isAuthenticated() || isAuthorized())) {
        return unauthenticated();
      }
      const {slotId, seats, user} = body;
      return ok(dbBooking.bookTicket(slotId, seats, user));
    }

    function getHistory() {
      if (!(isAuthenticated() || isAuthorized())) {
        return unauthenticated();
      }
      return ok(dbBooking.getBookingHistory(idFromUrl()));
    }

    function getMovieList() {
      if (!isAuthorized()) {
        return unauthorized();
      }
      return ok(dbMovie.getAllMovies());
    }

    function getTheatres() {
      if (!isAuthorized()) {
        return unauthorized();
      }
      return ok(dbTheatre.getAllTheatres());
    }

    function createMovie() {
      if (!isAuthorized()) {
        return unauthorized();
      }
      const name = body;
      return ok(dbMovie.add(name));
    }

    function createTheatre() {
      if (!isAuthorized()) {
        return unauthorized();
      }
      const theatre = body;
      return ok(dbTheatre.add(theatre));
    }

    function getShows() {
      if (!isAuthorized()) {
        return unauthorized();
      }

      const temp1 = url.split('=');
      const audiId = temp1[2];
      const temp2 = temp1[1];
      const theatreId = temp2.split('&')[0];

      return ok(dbShowTimings.getAllAudiShows(theatreId, audiId));
    }

    function saveShows() {
      if (!isAuthorized()) {
        return unauthorized();
      }
      const shows = body;
      return ok(dbShowTimings.saveShows(shows));
    }

    function ok(body?) {
      return of(new HttpResponse({status: 200, body}));
    }

    function unauthorized() {
      return throwError({status: 401, error: {message: 'Unauthorised'}});
    }

    function unauthenticated() {
      return throwError({status: 403, error: {message: 'Invalid Username/Password'}});
    }

    function idFromUrl() {
      const urlParts = url.split('=');
      return parseInt(urlParts[urlParts.length - 1]);
    }

    function isAuthenticated() {
      return headers.get('Authorization') === `Bearer fake-jwt-token-${Role.User}`;
    }

    function isAuthorized() {
      return headers.get('Authorization') === `Bearer fake-jwt-token-${Role.Admin}`;
    }
  }
}
