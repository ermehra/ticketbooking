import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule, routingComponents} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {Server} from '@app/interceptors/server';
import {DbModule} from '@app/modules/db/db.module';
import {ErrorInterceptor} from '@app/interceptors/error.interceptor';
import {MomentTimePipe} from './pipes/moment-time.pipe';
import { MomentDateTimePipe } from './pipes/moment-date-time.pipe';
import {JwtTokenInterceptor} from '@app/interceptors/jwt-token.interceptor';
import {BsDatepickerModule} from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ...routingComponents,
    MomentTimePipe,
    MomentDateTimePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    DbModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtTokenInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: Server, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
