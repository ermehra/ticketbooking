import {Theatre, Auditorium, Movie} from 'src/app/modules/db/fakeDbModel';
import {Moment} from 'moment';

export class ShowTiming {
  id: number;
  theatre: Theatre;
  auditorium: Auditorium;
  movie: Movie;

  /* Date and time of screening*/
  dateTime: Moment;

  /* ToDO: from and to dates of movies*/

  constructor(obj) {
    Object.assign(this, obj);
  }
}
