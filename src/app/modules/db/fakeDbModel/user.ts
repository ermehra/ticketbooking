export class User {
  id: number;
  name: string;
  username: string;
  password: string;
  type: Role;

  constructor(obj) {
    Object.assign(this, obj);
  }
}

export enum Role {
  User, Admin
}
