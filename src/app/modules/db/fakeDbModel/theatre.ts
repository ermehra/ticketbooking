import {Auditorium} from '@app/modules/db/fakeDbModel/auditorium';

export class Theatre {
  id: number;
  name: string;
  auditoriums: Auditorium[];

  constructor(obj) {
    Object.assign(this, obj);
  }
}
