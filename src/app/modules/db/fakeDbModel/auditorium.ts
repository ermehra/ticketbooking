export class Auditorium {
  id: number;
  name: string;
  seats: number;

  constructor(obj) {
    Object.assign(this, obj);
  }
}
