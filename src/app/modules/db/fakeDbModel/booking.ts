import {ShowTiming, User} from 'src/app/modules/db/fakeDbModel';
import {Moment} from 'moment';

export class Booking {
  id: number;
  user: User;
  showTime: ShowTiming;
  seats: number;
  bookedOn: Moment;

  constructor(obj) {
    Object.assign(this, obj);
  }
}
