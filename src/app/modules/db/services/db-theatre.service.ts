import {Injectable} from '@angular/core';
import {Theatre} from '@app/modules/db/fakeDbModel/index';
import {DbAuditoriumService} from '@app/modules/db/services/db-auditorium.service';

@Injectable({
  providedIn: 'root'
})
export class DbTheatreService {

  theatres: Theatre[] = [
    new Theatre({
      id: 1,
      name: 'Cinepolis',
      auditoriums: [this._dbAuditorium.getAuditorium(1), this._dbAuditorium.getAuditorium(2), this._dbAuditorium.getAuditorium(3)]
    }),
    new Theatre({
      id: 2,
      name: 'Wave Cinemas',
      auditoriums: [this._dbAuditorium.getAuditorium(4), this._dbAuditorium.getAuditorium(5)]
    }),
    new Theatre({
      id: 3,
      name: 'PVR Multiplex',
      auditoriums: [this._dbAuditorium.getAuditorium(6), this._dbAuditorium.getAuditorium(7), this._dbAuditorium.getAuditorium(8)]
    })
  ];

  constructor(private _dbAuditorium: DbAuditoriumService) {
  }

  add(theatre: Theatre) {
    const maxId = this.theatres.reduce((max, movie) => max > movie.id ? max : movie.id, 0);
    theatre.id = maxId + 1;
    theatre.auditoriums.forEach(audi => {
      // Adding audi in audi service
      this._dbAuditorium.add(audi);
    });
    this.theatres.push(theatre);
  }

  getTheatre(id: number) {
    return this.theatres.find(theatre => theatre.id === id);
  }

  getAllTheatres() {
    return this.theatres;
  }

  removeTheatre(id: number) {
    this.theatres = this.theatres.filter(theatre => theatre.id !== id);
  }
}
