import {Injectable} from '@angular/core';
import {ShowTiming} from '@app/modules/db/fakeDbModel';
import {DbTheatreService} from 'src/app/modules/db/services/db-theatre.service';
import {DbAuditoriumService} from 'src/app/modules/db/services/db-auditorium.service';
import {DbMovieService} from 'src/app/modules/db/services/db-movie.service';

import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DbShowTimingsService {

  showTimings: ShowTiming[] = [
    new ShowTiming({
      id: 1,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(1),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(9).minute(30)
    }),
    new ShowTiming({
      id: 2,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(1),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(14).minute(0)
    }),
    new ShowTiming({
      id: 3,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(1),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(17).minute(30)
    }),

    new ShowTiming({
      id: 4,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(2),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(18).minute(15)
    }),
    new ShowTiming({
      id: 5,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(2),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(20).minute(45)
    }),

    new ShowTiming({
      id: 6,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(3),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(10).minute(45)
    }),
    new ShowTiming({
      id: 7,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(3),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(16).minute(0)
    }),

    new ShowTiming({
      id: 8,
      theatre: this._dbTheatre.getTheatre(2),
      auditorium: this._dbAudi.getAuditorium(4),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(10).minute(0)
    }),
    new ShowTiming({
      id: 9,
      theatre: this._dbTheatre.getTheatre(2),
      auditorium: this._dbAudi.getAuditorium(4),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(17).minute(0)
    }),
    new ShowTiming({
      id: 10,
      theatre: this._dbTheatre.getTheatre(2),
      auditorium: this._dbAudi.getAuditorium(4),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(21).minute(30)
    }),
    new ShowTiming({
      id: 11,
      theatre: this._dbTheatre.getTheatre(2),
      auditorium: this._dbAudi.getAuditorium(4),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(22).minute(0)
    }),

    new ShowTiming({
      id: 12,
      theatre: this._dbTheatre.getTheatre(2),
      auditorium: this._dbAudi.getAuditorium(5),
      movie: this._dbMovie.getMovie(3),
      dateTime: moment().hour(11).minute(30)
    }),
    new ShowTiming({
      id: 13,
      theatre: this._dbTheatre.getTheatre(2),
      auditorium: this._dbAudi.getAuditorium(5),
      movie: this._dbMovie.getMovie(3),
      dateTime: moment().hour(17).minute(45)
    }),

    new ShowTiming({
      id: 14,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(6),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(11).minute(0)
    }),
    new ShowTiming({
      id: 15,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(6),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(18).minute(0)
    }),
    new ShowTiming({
      id: 16,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(6),
      movie: this._dbMovie.getMovie(1),
      dateTime: moment().hour(21).minute(0)
    }),

    new ShowTiming({
      id: 17,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(7),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(10).minute(30)
    }),
    new ShowTiming({
      id: 18,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(7),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(15).minute(30)
    }),
    new ShowTiming({
      id: 19,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(7),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(21).minute(0)
    }),

    new ShowTiming({
      id: 20,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(8),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(11).minute(40)
    }),
    new ShowTiming({
      id: 21,
      theatre: this._dbTheatre.getTheatre(3),
      auditorium: this._dbAudi.getAuditorium(8),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(10).minute(40)
    }),
    new ShowTiming({
      id: 22,
      theatre: this._dbTheatre.getTheatre(1),
      auditorium: this._dbAudi.getAuditorium(1),
      movie: this._dbMovie.getMovie(2),
      dateTime: moment().hour(10).minute(40).add(1,'day')
    })
  ];

  constructor(private _dbTheatre: DbTheatreService,
              private _dbAudi: DbAuditoriumService,
              private _dbMovie: DbMovieService) {
  }

  getAllShows() {
    return this.showTimings;
  }

  getAllShowsForDate(date: Date) {
    return this.showTimings.filter(showtime => moment(showtime.dateTime.toString()).isSame(date, 'day'));
  }

  getShow(id: number) {
    return this.showTimings.find(show => show.id === id);
  }

  addShow(showTiming: ShowTiming) {
    // find max id
    const maxId = this.showTimings.reduce((max, show) => max > show.id ? max : show.id, 0);

    showTiming.id = maxId + 1;
    this.showTimings.push(showTiming);
  }

  removeShow(showId: number) {
    this.showTimings = this.showTimings.filter(show => show.id !== showId);
  }

  getAllAudiShows(theatreId: string, audiId: string) {
    let result = {};

    const tId: number = parseInt(theatreId, 10);
    const aId: number = parseInt(audiId, 10);

    const filteredShows = this.showTimings.filter(show => show.theatre.id === tId && show.auditorium.id === aId);

    if (filteredShows.length > 0) {
      result = {
        'theatre': filteredShows[0].theatre,
        'audi': filteredShows[0].auditorium,
        'days': [...this.getFilteredDays(filteredShows)]
      };
    } else {
      result = {
        'theatre': this._dbTheatre.getTheatre(tId),
        'audi': this._dbAudi.getAuditorium(aId),
        'days': []
      };
    }

    return result;
  }

  getFilteredDays(filteredShows: ShowTiming[]) {
    const result = [];

    const map = new Map();
    filteredShows.forEach(show => {

      if (!map.has(show.dateTime.format('DDMMYYYY'))) {
        map.set(show.dateTime.format('DDMMYYYY'), true);

        console.log('while getting : ');
        console.log(show.dateTime);
        console.log(show.dateTime.format('DD/MM/YYYY').toString());

        result.push({
          movieId: show.movie.id,
          date: show.dateTime.format('DD/MM/YYYY'),
          shows: [...this.getFilteredShows(filteredShows, show.dateTime)]
        });
      }
    });
    return result;
  }

  getFilteredShows(filteredShows, datetime) {
    const result = [];

    filteredShows.forEach(show => {
      if (show.dateTime.isSame(datetime, 'day')) {
        result.push({
          time: show.dateTime.format('hh:mm A')
        });
      }
    });

    return result;
  }

  // This function will add new entries and if an entry with same time exists then retains it.
  saveShows(shows: any) {
    console.log(shows);
    const result: ShowTiming[] = [];

    // filter list with current audi and theatre
    const existingShows = this.showTimings.filter(show => show.auditorium.id === shows.audiId);

    // filter list without current audi and theatre
    const filteredShows: ShowTiming[] = this.showTimings
      .filter(show => show.auditorium.id !== shows.audiId);

    // get Max ID
    let maxId = this.showTimings.reduce((max, show) => max > show.id ? max : show.id, 0);

    shows.days.forEach(day => {
      day.shows.forEach(show => {

        let showDate = '';
        if (typeof day.date === 'string') {
          showDate = day.date;
        } else {
          showDate = moment(day.date).format('DD/MM/YYYY');
        }

        const dateTime = moment(showDate + ' ' + show.time, 'DD/MM/YYYY hh:mm A');

        // check if show exists in main list
        const existingShowTime = existingShows.find(s => s.dateTime.isSame(dateTime, 'minute'));
        if (existingShowTime) {
          filteredShows.push(existingShowTime);
        } else {
          // Add new show in filtered shows array.
          const theatre = this._dbTheatre.getTheatre(shows.theatreId);
          const audi = this._dbAudi.getAuditorium(shows.audiId);
          const movie = this._dbMovie.getMovie(day.movieId);

          filteredShows.push(new ShowTiming({id: ++maxId, theatre: theatre, auditorium: audi, movie: movie, dateTime: dateTime}));
        }

      });
    });

    this.showTimings = filteredShows;
    return true;
  }
}

/*
 'theatre': {id: 1, name: 'Cinepolis'},
    'audi': {id: 1, name: 'Audi 1'},
    'days': [
      {
        movieId: 1,
        date: '12/6/2019',
        shows: [
          {
            id: 1,
            time: '12:30 PM'
          },
          {
            id: 1,
            time: '1:00 PM'
          }]
      },
      {
        movieId: 2,
        date: '13/6/2019',
        shows: [
          {
            id: 1,
            time: '2:30 PM'
          },
          {
            id: 1,
            time: '5:00 PM'
          }]
      }]
*/
