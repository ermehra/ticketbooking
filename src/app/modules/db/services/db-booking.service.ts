import {Injectable} from '@angular/core';
import {Booking, ShowTiming, User} from '@app/modules/db/fakeDbModel';
import {DbShowTimingsService} from '@app/modules/db/services/db-show-timings.service';
import {current} from 'codelyzer/util/syntaxKind';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DbBookingService {

  bookings: Booking[] = [];

  constructor(private _showTiming: DbShowTimingsService) {
  }

  bookTicket(slotId: number, noOfSeats: number, user: User) {

    noOfSeats = +noOfSeats;

    // Get total number of seats
    const slotInfo = this._showTiming.getShow(slotId);

    if (noOfSeats > slotInfo.auditorium.seats) {
      throw new Error('Seats not available');
    }

    // Get all booked seats for that slot yet.
    const bookedSeats = this.bookings
      .filter(slot => slot.showTime.id === slotId)
      .reduce((prev, curr) => prev + (curr['seats'] || 0), 0);

    if (noOfSeats > (slotInfo.auditorium.seats - bookedSeats)) {
      throw new Error('Seats not available');
    }

    // const maxId = this.bookings.reduce( (prev, curr) => (prev['id'] > curr.id) ? prev : curr, 1);
    const maxId = this.bookings.reduce((prev, curr) => (prev['id'] > curr.id) ? prev : curr, {id: 0});

    const booking = new Booking({id: maxId.id + 1, user: user, showTime: slotInfo, seats: noOfSeats, bookedOn: moment()});
    this.bookings.push(booking);
    return true;
  }

  getBookingHistory(userId: number) {
    return this.bookings.filter(booking => booking.user.id === userId);
  }
}
