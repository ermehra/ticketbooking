import {Injectable} from '@angular/core';
import {Auditorium} from '@app/modules/db/fakeDbModel';

@Injectable({
  providedIn: 'root'
})
export class DbAuditoriumService {

  /* Seed values*/
  auditoriums: Auditorium[] = [
    new Auditorium({id: 1, name: 'Audi 1', seats: 20}),
    new Auditorium({id: 2, name: 'Audi 2', seats: 100}),
    new Auditorium({id: 3, name: 'Audi 3', seats: 50}),
    new Auditorium({id: 4, name: 'Audi 1', seats: 20}),
    new Auditorium({id: 5, name: 'Audi 2', seats: 120}),
    new Auditorium({id: 6, name: 'Audi 1', seats: 20}),
    new Auditorium({id: 7, name: 'Audi 2', seats: 120}),
    new Auditorium({id: 8, name: 'Audi 3', seats: 220})
  ];

  constructor() {
  }

  add(auditorium: Auditorium) {
    const maxId = this.auditoriums.reduce((max, audi) => max > audi.id ? max : audi.id, 0);
    auditorium.id = maxId + 1;
    this.auditoriums.push(auditorium);
    return auditorium;
  }

  getAuditorium(id: number) {
    return this.auditoriums.find(auditorium => auditorium.id === id);
  }

  getAllAuditoriums() {
    return this.auditoriums;
  }

  removeAuditorium(id: number) {
    this.auditoriums = this.auditoriums.filter(auditorium => auditorium.id !== id);
  }
}
