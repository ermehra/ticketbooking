import {Injectable} from '@angular/core';
import {Role, User} from '@app/modules/db/fakeDbModel';

@Injectable({
  providedIn: 'root'
})
export class DbUserService {

  users: User[] = [
    new User({id: 1, name: 'user', username: 'user@user.com', password: 'user', type: Role.User}),
    new User({id: 2, name: 'admin', username: 'admin@admin.com', password: 'admin', type: Role.Admin})
  ];

  constructor() {
  }

  addUser(user) {
    this.users.push(user);
  }

  getAllUsers() {
    return this.users;
  }

  getUser(id: number): User {
    return this.users.find(user => user.id === id);
  }

  getUserByNameAndPassword(username: string, password: string) {
    return this.users.find(user => user.username === username && user.password === password);
  }
}
