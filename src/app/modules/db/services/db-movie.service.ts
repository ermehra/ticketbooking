import {Injectable} from '@angular/core';
import {Movie} from '@app/modules/db/fakeDbModel';

@Injectable({
  providedIn: 'root'
})
export class DbMovieService {

  movies: Movie[] = [
    new Movie({id: 1, name: 'Avengers'}),
    new Movie({id: 2, name: 'Cashback'}),
    new Movie({id: 3, name: 'Three Ghosts'}),
  ];

  constructor() {
  }

  add(name: String) {
    // Find max id
    const maxId = this.movies.reduce((max, movie) => max > movie.id ? max : movie.id, 0);
    this.movies.push(new Movie({id: maxId + 1, name: name}));
    return maxId;
  }

  getMovie(id: number) {
    return this.movies.find(movie => movie.id === +id);
  }

  getAllMovies() {
    return this.movies;
  }

  removeMovie(id: number) {
    this.movies = this.movies.filter(movie => movie.id !== id);
  }
}
