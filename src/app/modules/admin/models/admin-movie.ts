export class AdminMovie {
  id: number;
  name: string;

  constructor(obj) {
    Object.assign(this, obj);
  }
}
