import {Moment} from 'moment';

export class AdminSlot {
  id: number;
  dateTime: Moment;

  constructor(obj) {
    Object.assign(this, obj);
  }
}
