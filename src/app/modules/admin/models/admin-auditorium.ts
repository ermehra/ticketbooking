import {AdminMovie} from '@app/modules/admin/models/admin-movie';
import {AdminSlot} from '@app/modules/admin/models/admin-slot';

export class AdminAuditorium {
  id: number;
  name: string;
  seats: number;
  movie: AdminMovie;
  showTimes: AdminSlot[];

  constructor(obj) {
    Object.assign(this, obj);
  }
}
