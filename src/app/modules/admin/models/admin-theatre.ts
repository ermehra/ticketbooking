import {AdminAuditorium} from '@app/modules/admin/models/admin-auditorium';

export class AdminTheatre {
  id: number;
  name: string;
  auditoriumns: AdminAuditorium[];

  constructor(obj) {
    Object.assign(this, obj);
  }
}
