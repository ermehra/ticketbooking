import {Pipe, PipeTransform} from '@angular/core';
import {AdminSlot} from '@app/modules/admin/models';
import {forEach} from '@angular/router/src/utils/collection';

@Pipe({
  name: 'timeConcat'
})
export class TimeConcatPipe implements PipeTransform {

  transform(slots: AdminSlot[], args?: any): any {
    const result: String[] = [];

    if (slots) {
      slots.forEach(slot => {
        result.push(slot.dateTime.format('h:mm a'));
      });
    }
    return result.join(', ');
  }
}
