import {Pipe, PipeTransform} from '@angular/core';
import {AdminMovie} from '@app/modules/admin/models';

@Pipe({
  name: 'movieNameFilter'
})
export class MovieNameFilterPipe implements PipeTransform {

  transform(movies: AdminMovie[], searchTerm) {
    if (!movies || !searchTerm) {
      return movies;
    }
    return movies.filter(movie => movie.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
  }

}
