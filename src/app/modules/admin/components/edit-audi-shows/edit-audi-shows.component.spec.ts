import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAudiShowsComponent } from './edit-audi-shows.component';

describe('EditAudiShowsComponent', () => {
  let component: EditAudiShowsComponent;
  let fixture: ComponentFixture<EditAudiShowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAudiShowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAudiShowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
