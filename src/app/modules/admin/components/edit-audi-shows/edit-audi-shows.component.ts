import {Component, OnInit} from '@angular/core';
import {ShowTimeService} from '@app/modules/admin/services/show-time.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MovieService} from '@app/modules/admin/services/movie.service';
import {AdminMovie} from '@app/modules/admin/models';
import {BsDatepickerConfig} from 'ngx-bootstrap';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-audi-shows',
  templateUrl: './edit-audi-shows.component.html',
  styleUrls: ['./edit-audi-shows.component.css']
})
export class EditAudiShowsComponent implements OnInit {

  showTimes: FormGroup;
  datePickerConfig: Partial<BsDatepickerConfig>;

  showTimesModel = {};

  movies: AdminMovie[] = [];
  theatreId: number;
  audiId: number;

  constructor(private _showService: ShowTimeService,
              private _activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private _movieService: MovieService,
              private _router: Router) {

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false,
      minDate: new Date(),
      dateInputFormat: 'DD/MM/YYYY'
    });
  }

  ngOnInit() {
    this.theatreId = parseInt(this._activatedRoute.snapshot.paramMap.get('theatreId'), 10);
    this.audiId = parseInt(this._activatedRoute.snapshot.paramMap.get('audiId'), 10);

    this._showService
      .getShows(this._activatedRoute.snapshot.paramMap.get('theatreId'), this._activatedRoute.snapshot.paramMap.get('audiId'))
      .subscribe(resp => {
        this.showTimesModel = resp;
        this.loadDataToForm();
      });

    this._movieService.getMovies().subscribe(resp => this.movies = resp);

    this.showTimes = this.fb.group({
      theatreName: [''],
      audiName: [''],
      days: this.fb.array([
        this.getDayControls()
      ])
    });
  }

  getDayControls() {
    return this.fb.group({
      movieId: ['', Validators.required],
      date: ['', Validators.required],
      times: this.fb.array([
        this.getTimeControls()
      ])
    });
  }

  getTimeControls() {
    return this.fb.group({
      time: ['', Validators.required]
    });
  }

  get daysArray(): FormArray {
    return <FormArray>this.showTimes.get('days');
  }

  addDateControls() {
    (<FormArray>this.showTimes.get('days')).push(this.getDayControls());
  }

  removeDateControls(index) {
    (<FormArray>this.showTimes.get('days')).removeAt(index);
  }

  addTimeControl(index) {
    (<FormArray>(<FormGroup>this.daysArray.controls[index]).controls.times).push(this.getTimeControls());
  }

  removeTimesControl(parentIndex, index) {
    (<FormArray>(<FormGroup>this.daysArray.controls[parentIndex]).controls.times).removeAt(index);
  }

  loadDataToForm() {
    this.showTimes.patchValue({
      theatreName: this.showTimesModel['theatre'].name,
      audiName: this.showTimesModel['audi'].name
    });

    this.showTimes.setControl('days', this.setExistingDays(this.showTimesModel['days']));

  }

  setExistingDays(days): FormArray {
    const formArray: FormArray = new FormArray([]);
    days.forEach(day => {
      const timesFormArray: FormArray = new FormArray([]);
      day.shows.forEach(t => {
        timesFormArray.push(this.fb.group({
          time: moment(t.time, 'hh:mm A').toDate()
        }));
      });
      formArray.push(this.fb.group({
        date: day.date,
        movieId: day.movieId,
        times: timesFormArray
      }));
    });
    return formArray;
  }

  saveShowTimes() {
    this._showService.saveShows(this.makeShowModelFromForm()).subscribe(resp => {
      this._router.navigate(['list'], {relativeTo: this._activatedRoute});
    });
  }

  makeShowModelFromForm() {
    const showModel = {
      'theatreId': this.theatreId,
      'audiId': this.audiId,
      days: [...this.getDaysArray()]
    };
    return showModel;
  }

  getDaysArray() {
    const result = [];

    this.showTimes.value.days.forEach(day => {

      result.push({
        'movieId': day.movieId,
        'date': day.date,
        'shows': [...this.getShows(day.times)]
      });
    });
    return result;
  }

  getShows(times) {
    const result = [];

    times.forEach(time => {
      result.push({
        'time': moment(time.time).format('hh:mm A')
      });
    });

    return result;
  }
}

/*
export interface showTimeDTO {
  'theatre': Theatre,
  'audi': {id: 1, name: 'Audi 1'},
  'days': [
    {
      movieId: 1,
      date: '12/6/2019',
      shows: [
        {
          id: 1,
          time: '12:30 PM'
        },
        {
          id: 1,
          time: '1:00 PM'
        }]
    },
    {
      movieId: 2,
      date: '13/6/2019',
      shows: [
        {
          id: 1,
          time: '2:30 PM'
        },
        {
          id: 1,
          time: '5:00 PM'
        }]
    }]
}*/
