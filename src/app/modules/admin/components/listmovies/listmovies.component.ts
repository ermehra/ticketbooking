import {Component, OnInit} from '@angular/core';
import {MovieService} from '@app/modules/admin/services/movie.service';
import {AdminMovie} from '@app/modules/admin/models';
import {finalize} from 'rxjs/internal/operators';
import {MessageService} from '@app/services';

@Component({
  selector: 'app-listmovies',
  templateUrl: './listmovies.component.html',
  styleUrls: ['./listmovies.component.css']
})
export class ListmoviesComponent implements OnInit {

  movies: AdminMovie[];
  movieName;
  loading = false;
  searchMovie: string;

  constructor(private _movieService: MovieService, private _msgService: MessageService) {
  }

  ngOnInit() {
    this._movieService.getMovies().subscribe(resp => this.movies = resp);
  }

  createMovie() {
    this.loading = true;
    this._movieService.createMovie(this.movieName)
      .pipe(finalize(() => this.loading = false))
      .subscribe(resp => {
          this.movies.push(new AdminMovie({id: resp, name: this.movieName}));
          this.movieName = '';
          this._msgService.success('Movie created successfully !');
        },
        error => this._msgService.error('Movie could not be created'));
  }
}
