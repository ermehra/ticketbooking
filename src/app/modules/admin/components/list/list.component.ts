import {Component, OnInit} from '@angular/core';
import {TheatreService} from '@app/modules/admin/services';
import {AdminTheatre} from '@app/modules/admin/models';
import {ActivatedRoute, Router} from '@angular/router';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  theatres: AdminTheatre[] = [];
  selectedDate: Date = new Date();

  constructor(private _theatreService: TheatreService, private _router: Router, private _route: ActivatedRoute) {
    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false,
      minDate: new Date(),
      dateInputFormat: 'DD/MM/YYYY'
    });

  }

  ngOnInit() {
    this.loadTheatres(new Date());
  }

  private loadTheatres(date: Date) {
    this._theatreService.listTheatres2(date).subscribe(theatres => {
      this.theatres = theatres;
    });
  }

  goToEdit(theatreId, audiId) {
    this._router.navigate(['../editshows', theatreId, audiId], {relativeTo: this._route});
  }

  filterTheatre(event) {
    this.loadTheatres(event);
  }
}
