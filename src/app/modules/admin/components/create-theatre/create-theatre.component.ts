import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Auditorium, Theatre} from '@app/modules/db/fakeDbModel';
import {TheatreService} from '@app/modules/admin/services';
import {timepickerReducer} from 'ngx-bootstrap/timepicker/reducer/timepicker.reducer';
import {finalize} from 'rxjs/internal/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-create-theatre',
  templateUrl: './create-theatre.component.html',
  styleUrls: ['./create-theatre.component.css']
})
export class CreateTheatreComponent implements OnInit {

  theatreForm: FormGroup;
  loading = false;

  constructor(private fb: FormBuilder,
              private _theatreService: TheatreService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.theatreForm = this.fb.group({
      theatreName: ['', Validators.required],
      audis: this.fb.array([
        this.addAudiGroup()
      ])
    });
  }

  addAudiGroup(): FormGroup {
    return this.fb.group({
      audiName: ['', Validators.required],
      seats: ['', Validators.required]
    });
  }

  onAddAudi() {
    (<FormArray>this.theatreForm.get('audis')).push(this.addAudiGroup());
  }

  onSubmit() {
    this.loading = true;
    this._theatreService.createTheatre(this.getTheatre())
      .pipe(finalize(() => this.loading = false))
      .subscribe(resp => {
        this._router.navigate(['list'], {relativeTo: this._activatedRoute});
      });
  }

  getTheatre() {
    const theatre: Theatre = new Theatre({id: null, name: this.theatreForm.value.theatreName, auditoriums: []});

    this.theatreForm.value.audis.forEach(audi => {
      theatre.auditoriums.push(new Auditorium({id: null, name: audi.audiName, seats: audi.seats}));
    });

    return theatre;
  }
}
