import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from './admin-routing.module';
import {TimeConcatPipe} from './pipes/time-concat.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  CreateTheatreComponent,
  EditAudiShowsComponent,
  HomeComponent,
  ListComponent,
  ListmoviesComponent
} from '@app/modules/admin/components';
import {BsDatepickerModule, TimepickerModule} from 'ngx-bootstrap';
import { MovieNameFilterPipe } from './pipes/movie-name-filter.pipe';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  declarations: [ListComponent, TimeConcatPipe, HomeComponent, ListmoviesComponent, CreateTheatreComponent, EditAudiShowsComponent, MovieNameFilterPipe]
})
export class AdminModule {
}
