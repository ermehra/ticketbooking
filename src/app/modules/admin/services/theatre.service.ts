import {Injectable} from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import * as fakeDb from '@app/modules/db/fakeDbModel';
import {ShowTiming} from '@app/modules/db/fakeDbModel';
import {API} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AdminAuditorium, AdminMovie, AdminSlot, AdminTheatre} from '@app/modules/admin/models';

@Injectable({
  providedIn: 'root'
})
export class TheatreService {

  constructor(private _http: HttpClient) {
  }

  createTheatre(theatre: fakeDb.Theatre) {
    return this._http.post(API.createTheatre, theatre);
  }

  listTheatres2(filterDate: Date) {
    return this._http.post<fakeDb.ShowTiming[]>(API.adminMovies, filterDate).pipe(switchMap(resp => {

      const showTimings: ShowTiming[] = resp;
      const result: AdminTheatre[] = [];
      this._http.get<fakeDb.Theatre[]>(API.theatre).subscribe(theatres => {
        theatres.forEach(theatre => {
          const adminTheatre: AdminTheatre = new AdminTheatre({});
          adminTheatre.id = theatre.id;
          adminTheatre.name = theatre.name;

          theatre.auditoriums.forEach(audi => {
            const adminAudi: AdminAuditorium = new AdminAuditorium({});
            adminAudi.id = audi.id;
            adminAudi.name = audi.name;
            adminAudi.seats = audi.seats;

            // Get all shows for this audi
            showTimings.forEach(show => {
              if (show.auditorium.id === audi.id) {
                const adminMovie: AdminMovie = new AdminMovie({});
                adminMovie.id = show.movie.id;
                adminMovie.name = show.movie.name;

                const adminSlot: AdminSlot = new AdminSlot({});
                adminSlot.id = show.id;
                adminSlot.dateTime = show.dateTime;

                if (adminAudi.showTimes) {
                  adminAudi.showTimes.push(adminSlot);
                } else {
                  adminAudi.showTimes = [adminSlot];
                }

                adminAudi.movie = adminMovie;
              }
            });
            if (adminTheatre.auditoriumns) {
              adminTheatre.auditoriumns.push(adminAudi);
            } else {
              adminTheatre.auditoriumns = [adminAudi];
            }
          });
          result.push(adminTheatre);
        });
      });

      return of(result);
    }));
  }


  /*
  @Depricated
  listTheatres() {
    return this._http.get<fakeDb.ShowTiming[]>(API.adminMovies).pipe(switchMap(value => {
      const result: AdminTheatre[] = [];

      value.forEach(show => {

        const slot: AdminSlot = new Slot({id: show.id, dateTime: show.dateTime});

        const adminMovie: AdminMovie = new AdminMovie({id: show.movie.id, name: show.movie.name});
        const auditorium: AdminAuditorium = new AdminAuditorium({
          id: show.auditorium.id, name: show.auditorium.name,
          seats: show.auditorium.seats, movie: adminMovie, showTimes: [slot]
        });

        const theatreIndex = result.findIndex(t => t.id === show.theatre.id);
        if (theatreIndex > -1) {

          /!* Check if movie already exists in object*!/
          const audiIndex = result[theatreIndex].auditoriumns.findIndex(audi => audi.id === show.auditorium.id);
          if (audiIndex > -1) {
            /!* existing audi*!/
            if (result[theatreIndex].auditoriumns[audiIndex].movie.id === show.movie.id) {
              /!* Existing movie*!/
              result[theatreIndex].auditoriumns[audiIndex].showTimes.push(slot);
            } else {
              /!* New Movie*!/
              result[theatreIndex].auditoriumns[audiIndex].movie = adminMovie;
              result[theatreIndex].auditoriumns[audiIndex].showTimes = [slot];
            }
          } else {
            /!* New Audi*!/
            result[theatreIndex].auditoriumns.push(auditorium);
          }
        } else {
          /!* New theatre*!/
          const theatre = new AdminTheatre({id: show.theatre.id, name: show.theatre.name, auditoriumns: [auditorium]});
          result.push(theatre);
        }
      });
      console.log(result);
      return of(result);
    }));
  }
*/

}
