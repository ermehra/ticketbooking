import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {API} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShowTimeService {

  constructor(private _http: HttpClient) { }

  /* This will get all the shows for all dates */
  getShows(theatreId, audiId) {
     return this._http.get(`${API.shows}?theatreId=${theatreId}&audiId=${audiId}`);
  }

  saveShows(shows) {
    return this._http.post(API.shows, shows);
  }
}
