import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API} from '../../../../environments/environment';
import {AdminMovie} from '@app/modules/admin/models';
import {map} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private _http: HttpClient) {
  }

  getMovies() {
    return this._http.get<AdminMovie[]>(API.listMovies).pipe(map(resp => {
      const adminMovies: AdminMovie[] = [];
      resp.forEach(movie => adminMovies.push(new AdminMovie({id: movie.id, name: movie.name})));
      return adminMovies;
    }));
  }

  createMovie(movie: String) {
    return this._http.post(API.createMovie, movie);
  }

}
