import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {
  CreateTheatreComponent,
  EditAudiShowsComponent,
  HomeComponent,
  ListComponent,
  ListmoviesComponent
} from '@app/modules/admin/components';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home',
    component: HomeComponent,
    children : [
      {path: '', redirectTo: 'list', pathMatch: 'full'},
      {path: 'list', component: ListComponent},
      {path: 'createTheatre', component: CreateTheatreComponent},
      {path: 'editshows/:theatreId/:audiId', component: EditAudiShowsComponent},
      {path: 'listmovies', component: ListmoviesComponent},
      {path: '**', redirectTo: 'list', pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
