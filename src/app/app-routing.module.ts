import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthenticationGuard} from '@app/guards/authentication.guard';
import {AuthorizationGuard} from '@app/guards/authorization.guard';
import {LoginComponent} from '@app/components/login/login.component';
import {MoviesComponent} from '@app/components/movies/movies.component';
import {BookTicketComponent} from '@app/components/book-ticket/book-ticket.component';
import {HistoryComponent} from '@app/components/history/history.component';
import {PageNotFoundComponent} from '@app/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', redirectTo: 'movies', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'movies', component: MoviesComponent, canActivate: [AuthenticationGuard]},
  {path: 'bookticket/:id', component: BookTicketComponent, canActivate: [AuthenticationGuard]},
  {path: 'history', component: HistoryComponent, canActivate: [AuthenticationGuard]},
  {path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule', canActivate: [AuthorizationGuard]},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [
  LoginComponent,
  PageNotFoundComponent,
  MoviesComponent,
  BookTicketComponent,
  HistoryComponent
];
