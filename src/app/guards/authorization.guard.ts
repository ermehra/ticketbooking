import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {LoginService, MessageService} from '@app/services';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {
  constructor(private _loginService: LoginService, private _messageService: MessageService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    // clear all prev messages
    this._messageService.clear();

    if (this._loginService.isAdmin()) {
      return true;
    } else {
      return false;
    }
  }
}
