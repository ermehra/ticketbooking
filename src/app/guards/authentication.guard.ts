import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {LoginService, MessageService} from '@app/services';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(private _loginService: LoginService, private _route: Router, private _messageService: MessageService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    // clear all prev messages
    this._messageService.clear();

    if (this._loginService.getLoggedUserValue()) {
      return true;
    } else {
      this._route.navigate(['login'], {queryParams: {url: state.url}});
      return false;
    }
  }
}
