import { Component } from '@angular/core';
import {User} from './modules/db/fakeDbModel/user';
import {MessageService, LoaderService, LoginService} from '@app/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  loggedIn: User;
  showloader: boolean;
  message;

  constructor(private _loginService: LoginService, private _loader: LoaderService, private _msgService: MessageService) {

    this._loginService.loggedUserObservable.subscribe(resp => this.loggedIn = resp);
    this._loader.loaderObservable.subscribe(resp => this.showloader = resp);
    this._msgService.getMessage().subscribe((msg => this.message = msg));
  }

}
