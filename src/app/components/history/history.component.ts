import {Component, OnInit} from '@angular/core';
import {BookingService, MessageService} from '@app/services';
import {Booking} from '@app/modules/db/fakeDbModel';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  bookings: Booking[] = [];

  constructor(private _bookingService: BookingService, private _msgService: MessageService) {
  }

  ngOnInit() {
    this._bookingService.getBookingHistory().subscribe(resp => {
      this.bookings = resp;
      if (this.bookings.length === 0) {
        this._msgService.error('No History of tickets found');
      }
    });
  }

}
