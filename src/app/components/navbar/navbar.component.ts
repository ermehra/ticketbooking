import {Component, OnInit} from '@angular/core';
import {LoginService} from '@app/services';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isAdmin = false;

  constructor(private _loginService: LoginService) {
    this.isAdmin = this._loginService.isAdmin();
  }

  ngOnInit() {
  }

  logout() {
    this._loginService.logout();
  }

}
