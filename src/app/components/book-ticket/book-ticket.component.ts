import {Component, OnInit} from '@angular/core';
import {DbShowTimingsService} from '@app/modules/db/services/db-show-timings.service';
import {ShowTiming} from '@app/modules/db/fakeDbModel';
import {ActivatedRoute} from '@angular/router';
import {BookingService} from '@app/services/booking.service';
import {MessageService} from '@app/services';
import {finalize} from 'rxjs/internal/operators';

@Component({
  selector: 'app-book-ticket',
  templateUrl: './book-ticket.component.html',
  styleUrls: ['./book-ticket.component.css']
})
export class BookTicketComponent implements OnInit {

  showTiming: ShowTiming;
  seats = 1;
  loading = false;

  constructor(private _showTiming: DbShowTimingsService,
              private _activeRoute: ActivatedRoute,
              private _msgService: MessageService,
              private _bookingService: BookingService) {
  }

  ngOnInit() {
    const slotId = parseInt(this._activeRoute.snapshot.paramMap.get('id'), 10);
    this.showTiming = this._showTiming.getShow(slotId);
  }

  bookTicket() {
    this.loading = true;
    this._msgService.clear();

    if (this.seats) {
      this._bookingService.bookTicket(this.showTiming.id, this.seats)
        .pipe(finalize(() => {
          this.loading = false;
        }))
        .subscribe(resp => {
            console.log('Booked');
            this._msgService.success('Tickets booked !!');
          },
          error => {
            this._msgService.error(error);
          });
    }
  }
}
