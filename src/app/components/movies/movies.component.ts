import {Component, OnInit} from '@angular/core';
import {MoviesService} from '@app/services/movies.service';
import {Theatre} from '@app/models';
import {Router} from '@angular/router';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  theatres: Theatre[];
  datePickerConfig: Partial<BsDatepickerConfig>;
  selectedDate: Date = new Date();

  constructor(private _movies: MoviesService, private _router: Router) {
    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false,
      minDate: new Date(),
      dateInputFormat: 'DD/MM/YYYY'
    });
  }

  ngOnInit() {
    this.getFilteredMovies(new Date());
  }

  private getFilteredMovies(date) {
    this._movies.getAllMovies(date).subscribe(theatres => {
      this.theatres = theatres;
    });
  }

  goToBookTicketPage(slotId: number) {
    console.log(slotId);
    this._router.navigate(['bookticket', slotId]);
  }

  filterTheatre(date) {
    this.getFilteredMovies(date);
  }
}
