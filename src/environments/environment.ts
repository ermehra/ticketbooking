// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

const BaseUrl = 'fakeUrl://api';

export const API = {
  'login': `${BaseUrl}/login`,
  'register': `${BaseUrl}/addUser`,
  'movies': `${BaseUrl}/movies`,
  'theatre': `${BaseUrl}/theatre`,
  'adminMovies': `${BaseUrl}/adminMovies`,
  'book': `${BaseUrl}/book`,
  'history': `${BaseUrl}/history`,
  'listMovies': `${BaseUrl}/listMovies`,
  'createMovie': `${BaseUrl}/createMovie`,
  'createTheatre': `${BaseUrl}/createTheatre`,
  'shows': `${BaseUrl}/shows`
};
