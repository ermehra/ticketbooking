export const environment = {
  production: true
};

const BaseUrl = 'fakeUrl://api';

export const API = {
  'login': `${BaseUrl}/login`,
  'register': `${BaseUrl}/addUser`,
  'movies': `${BaseUrl}/movies`
};
